import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HumanConfig {
    @Bean
    public Human HumanJohn(){
        return new Human("John",180);
    }
}
